# Visual Sensitivity Analysis Of Curves

This repository contains the supplemental materials for the article entitled
*“A visual sensitivity analysis for parameter-augmented ensembles of curves”*
(authored by A. Ribés, J. Pouderoux, and B. Iooss; published in the Journal
of Validation Verification and Uncertainty Quantification).

In order to facilitate the reproduction of the results, we have written a
short document: “A guide for reproducibility of results” that is also
included in this repository.

In the article three datasets illustrate our claims. Two synthetic examples
and one industrial use-case, we named them:
  1. Oscillating tangents
  2. Campbell 1D functions
  3. A hydraulics study-case

We include raw data, python scripts and detailed steps on how to reproduce
the images of the article for these 3 cases. One demonstration video,
in mp4 format, is available per dataset. The scripts were tested in ParaView
version 5.4.1, which contains all diagrams and functionalities necessary to
the reproduction of the figures of the article. Newer versions of ParaView
should also support these functionalities.
