# Functional toy function: Arctangent temporal function (Auder, 2011)
# X: input matrix (in [-7,7]^2)
# q: number of discretization steps of [0,2pi] interval
# output: vector of q values

atantemp <- function(X, q = 100){

  n <- dim(X)[[1]]
  t <- (0:(q-1)) * (2*pi) / (q-1)

  res <- matrix(0,ncol=q,nrow=n)
  for (i in 1:n) res[i,] <- atan(X[i,1]) * cos(t) + atan(X[i,2]) * sin(t)
 
  return(res) 
}

# Tests functional toy fct 
n <- 400
y0 <- atantemp(matrix(c(-7,0,7,-7,0,7),ncol=2))
#plot(y0[1,],type="l")
#apply(y0,1,lines)

X <- matrix(c(runif(2*n,-7,7)),ncol=2)
y <- atantemp(X)

x11()
plot(y0[2,],ylim=c(-2,2),type="l")
apply(y,1,lines)

write.csv(X, file = "oscillating_tangents_input_parameters.csv")
write.csv(y, file = "oscillating_tangents_output_curves.csv")

